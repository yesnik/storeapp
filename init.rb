require_relative 'app/store_application'

StoreApplication.config do |app|
  app.name = 'My Store'
  app.environment = :production

  app.admin do |admin|
    admin.email = 'admin@mystore.ru'
    admin.login = 'admin'
    admin.send_info_emails_on :mondays
  end  
end

p StoreApplication.environment
p StoreApplication.name
p StoreApplication::Admin.email
p StoreApplication::Admin.login
# p StoreApplication::Admin.send_info_emails_on

#cart = Cart.new
#cart.add_item VirtualItem.new(price: 100)
#cart.add_item RealItem.new(price: 5000, weight: 400)


#order = Order.new
#order.add_item VirtualItem.new(price: 100)
#order.add_item RealItem.new(price: 5000, weight: 400)
#order.add_item RealItem.new(weight: 300, price: 90)

@items = []
@items << AntiqueItem.new('car', price: 200, weight: 1200)
@items << RealItem.new('kettle', price: 150, weight: 193)
@items << RealItem.new('dishwasher', price:120, weight: 250)

cart = Cart.new('nik')
cart.add_item RealItem.new('car', price: 350, weight: 150)
cart.add_item RealItem.new('car', price: 450, weight: 180)
cart.add_item RealItem.new('kettle', price: 700, weight: 10)

# p cart.all_cars
# p cart.all_kettles

#p 'Placing order...'
order = Order.new
order.place
p order.placed_at


