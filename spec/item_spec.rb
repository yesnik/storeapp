require 'rspec'
require_relative '../app/item'
require_relative '../app/virtual_item'

describe Item do
  before do
    @item = Item.new('kettle', price: 200)
  end

  describe '#price' do
    it 'calculates price according to formula' do
      expect(@item.price).to eq 222.0
    end
  end

  describe '#to_s' do
    it do
      expect(@item.to_s).to eq 'kettle:222.0'
    end
  end

  describe '#tax' do
    it do
      expect(@item.send(:tax)).not_to be_nil
    end
  end
end
