require 'rspec'
require_relative '../app/item'
require_relative '../app/virtual_item'
require_relative '../app/item_container'

class ItemBox
  include ItemContainer 
end

describe ItemContainer do
  before do
    @box = ItemBox.new
    @item1 = Item.new('kettle', price: 200) 
    @item2 = Item.new('kettle', price: 500)
  end

  describe '#add_item' do
    it 'adds item to the box' do
      @box.add_item(@item1)
      @box.add_item(@item2)

      expect(@box.items).to match_array [@item1, @item2] 
    end
  end

  describe '#remove_item' do
    it 'removes item from the box' do
      @box.add_item(@item1)
      @box.add_item(@item2)

      expect(@box.remove_item).to be @item2

      @box.remove_item
      expect(@box.items).to be_empty
    end

    it 'raises error when we pass not Item object' do
      expect(lambda { @box.add_item('hello') }).to raise_error NoMethodError
    end
  end

  describe '#min_price' do
    it { expect(ItemBox.min_price).to eq 100 }
  end
end

