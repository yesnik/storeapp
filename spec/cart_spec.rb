require 'rspec'
require_relative '../app/item_container'
require_relative '../app/item'
require_relative '../app/antique_item'
require_relative '../app/virtual_item'
require_relative '../app/cart'

describe Cart do
  describe '#add_items' do
    it '' do
      cart = Cart.new('nik')
      item1 = Item.new('kettle', price: 200)
      item2 = Item.new('car', price: 300)
      cart.add_items(item1, item2)

      expect(cart.items).to include [item1, item2]
    end
  end

  it 'removes items from itself' do
  
  end
  
  it 'counts items in itself'
  it 'places order using all the items that were added into the cart'
  it 'clears itself off the items after an order is placed'

  describe '#' do

  end
end

