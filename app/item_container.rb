module ItemContainer

  def self.included(base)
    base.extend ClassMethods
    base.class_eval do
      include InstanceMethods
    end
  end  

  module ClassMethods
    def min_price
      100
    end
  end

  module InstanceMethods
    attr_reader :items

    def initialize
      @items = []
    end

    def method_missing(method_name)
      if method_name =~ /^all_/
        name = method_name.to_s.sub(/^all_/, '').chomp('s')
        show_all_items_with(name)
      else
        super
      end
    end

    def add_item(item)
      @items.push item if item.price >= self.class.min_price
    end

    def remove_item
      @items.pop
    end

    def delete_invalid_items
      @items.delete_if { |item| item.price.nil? }
    end

    def validate
      @items.each do |item|
        puts 'Item price cannot be blank' unless item.price
      end
    end

    def count_valid_items
      @items.count { |item| item.price }
    end

    private

    def show_all_items_with(name)
      @items.select { |item| item.name == name }
    end
  end
end

