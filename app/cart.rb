class Cart
  attr_reader :items

  include ItemContainer

  class ItemNotSupported < StandardError; end

  UNSUPPORTED_ITEMS = [VirtualItem, AntiqueItem]
  
  def initialize(owner)
    @items = []
    @owner = owner
  end

  def add_items(*items)
    @items << items
  end

  def save_to_file
    File.open("#{@owner}_cart.txt", 'w') do |f|
      @items.each do |item|
        raise ItemNotSupported if UNSUPPORTED_ITEMS.include? item.class
        f.puts item
      end
    end
  end

  def read_from_file
    # return unless File.exists?("#{@owner}_cart.txt")
    
    File.readlines("#{@owner}_cart.txt").each do |row|
      @items << row.to_real_item
    end

    @items.uniq!
  rescue Errno::ENOENT
    File.open("#{@owner}_cart.txt", 'w')
    puts "File `#{@owner}_cart.txt` was created."
  end
end


