class Item
  attr_reader :name, :real_price

  @@discount = 0.1

  def initialize(name, options={})
    @name = name || 'none'
    @real_price = options[:price]

    @attr_values = {}
  end

  def info
    @attr_values = @attr_values.merge({name: name, price: price})
    return @attr_values unless block_given?
    
    @attr_values.each do |k, v|
      yield(k, v)
    end
  end

  def self.discount
    if Time.now.month == 4
      @@discount + 0.2
    else
      @@discount
    end
  end

  def self.show_info_about(attr, block)
    @@show_info_about ||= {}
    @@show_info_about[attr] = block
  end

  def price
    (@real_price * (1 - self.class.discount)) + tax if @real_price
  end

  def price=(value)
    @real_price = value
  end

  def to_s
    "#{self.name}:#{self.price}"
  end

  private

  def tax
    type_tax = if self.is_a? VirtualItem
                 1
               else
                 2
               end

    cost_tax = if @real_price > 100
                 @real_price * 0.2
               else
                 @real_price * 0.1
               end

    type_tax + cost_tax
  end
end

# item = Item.new({name: 'Pen', weight: 5, price: 100})
# p item.info
# item.info { |attr| puts attr.to_s + '!!!' }
# puts item.price


