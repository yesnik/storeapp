class StoreApplication
  class << self
    attr_accessor :name, :environment

    def config
      yield(self) if block_given?

      return @instance if @instance

      require 'pony'

      require_relative 'string'
      require_relative 'item_container'
      require_relative 'order'
      require_relative 'item'
      require_relative 'antique_item'
      require_relative 'virtual_item'
      require_relative 'real_item'
      require_relative 'cart'
    
      @instance ||= self
      @instance.freeze
    end

    def admin(&block)
      @admin ||= Admin.new(&block)
    end    
  end

  class Admin
    # attr_accessor :name, :login
    
    class << self
      attr_accessor :email, :login

      def new
        yield(self) unless @admin
        @admin ||= self
        @admin.freeze
      end

      def send_info_emails_on(day)
        @send_info_emails_on = day
      end
    end
  end
end

