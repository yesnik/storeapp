require_relative 'item'

class RealItem < Item
  attr_reader :weight

  show_info_about :weight, lambda { |attr| attr > 5 if attr }

  def initialize(name, options={})
    @weight = options[:weight]
    super
  end

  def info
    # @attr_values.merge!({weight: weight})
    if !@@show_info_about[:weight] || @@show_info_about[:weight].call(weight)
      # yield weight
      @attr_values.merge!({weight: weight})
    end
    super
  end

  def to_s
    "#{super}:#{self.weight}"
  end
end

# item = RealItem.new({price: 100, weight: 200})
# item.info { |k, v| p "Attr: #{k} -- #{v}" }
# p item

