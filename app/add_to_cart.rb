require_relative 'init'


cart = Cart.new(ARGV.delete_at(0))

ARGV.each do |arg|
  @items.each do |item|
    cart.add_item(item) if item.name == arg    
  end
end

cart.read_from_file

begin
  cart.save_to_file
rescue Cart::ItemNotSupported
  puts 'Cart does not support saving this type of items to file.'
  puts "Unsupported items: #{Cart::UNSUPPORTED_ITEMS}"
end


